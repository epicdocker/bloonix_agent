<!-- vscode-markdown-toc -->
* 1. [Requirements](#Requirements)
* 2. [Example](#Example)
	* 2.1. [Go to the examples directory](#Gototheexamplesdirectory)
	* 2.2. [Create Docker network](#CreateDockernetwork)
	* 2.3. [Startup Bloonix server](#StartupBloonixserver)
	* 2.4. [WebUI Login](#WebUILogin)
	* 2.5. [Create host via WebUI](#CreatehostviaWebUI)
	* 2.6. [Update Bloonix agent configuration](#UpdateBloonixagentconfiguration)
	* 2.7. [Start Bloonix agent](#StartBloonixagent)
	* 2.8. [Create services to check _(optional)_](#Createservicestocheck_optional_)
	* 2.9. [Cleanup](#Cleanup)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->


# Simple example to add Bloonix agent


##  1. <a name='Requirements'></a>Requirements

- Docker installed - https://www.docker.com/get-docker
- Checked out directory `examples` - https://gitlab.com/epicdocker/bloonix_agent/tree/master/examples
- Linux CLI or Cygwin for MS Windows


##  2. <a name='Example'></a>Example


###  2.1. <a name='Gototheexamplesdirectory'></a>Go to the examples directory

    cd [bloonix agent directory]/examples


###  2.2. <a name='CreateDockernetwork'></a>Create Docker network

First of all, we will create our own Docker network for this example.

    docker network create example_bloonix_net


###  2.3. <a name='StartupBloonixserver'></a>Startup Bloonix server

Now we start the Bloonix server with all required containers.

    docker-compose -f simple_example_server.yml up -d

Before starting the container, the required images are downloaded, which may take a while depending on your internet connection.

Finally, this process should look like this:
    
    Creating volume "examples_example_bloonix_database_data" with default driver
    Creating volume "examples_example_bloonix_server_data" with default driver
    Creating volume "examples_example_bloonix_search_data" with default driver
    Pulling elasticsearch (docker.elastic.co/elasticsearch/elasticsearch:5.6.8)...
    5.6.8: Pulling from elasticsearch/elasticsearch
    af4b0a2388c6: Pull complete
    64c3898dafdc: Pull complete
    ec1181075d13: Pull complete
    6d7b56a0dd46: Pull complete
    b46e009fc309: Pull complete
    ed4897316262: Pull complete
    23895b23d904: Pull complete
    ee8bc449f517: Pull complete
    ecfefe5bb645: Pull complete
    f440b19c1749: Pull complete
    Digest: sha256:49319ae0acab8ac2092a3ea9dd17d812d68677733de47bea43c79db503135477
    Status: Downloaded newer image for docker.elastic.co/elasticsearch/elasticsearch:5.6.8
    Pulling postgres (registry.gitlab.com/epicdocker/bloonix_postgresql:latest)...
    latest: Pulling from epicdocker/bloonix_postgresql
    550fe1bea624: Pull complete
    04bf519c70df: Pull complete
    2af56c798cc1: Pull complete
    9ad5d4394eec: Pull complete
    df51d99b22c1: Pull complete
    b8d2552ec456: Pull complete
    f8ef74d64f40: Pull complete
    dbf7954a040a: Pull complete
    3250b956dc72: Pull complete
    24cf5586d651: Pull complete
    Digest: sha256:4ff1b8ee60db4556af2456c89e924e4c2ab380de1cec07a0743e88a61b582d3e
    Status: Downloaded newer image for registry.gitlab.com/epicdocker/bloonix_postgresql:latest
    Pulling server (registry.gitlab.com/epicdocker/bloonix_server:latest)...
    latest: Pulling from epicdocker/bloonix_server
    2a72cbf407d6: Pull complete
    3ca411a9085e: Pull complete
    a54c8badba37: Pull complete
    9cfb3b6a07e5: Pull complete
    c4a13f30b1f2: Pull complete
    6b1abd28eaed: Pull complete
    1d21e7d4352a: Pull complete
    8bdc5f3c2785: Pull complete
    3bce2935db85: Pull complete
    95884d8a6e2d: Pull complete
    Digest: sha256:64854a0beb1af372217cf585ed77afbcc378231ef1ea83cfc4099663e1ab51ea
    Creating example_bloonix_search ... done
    Creating example_bloonix_server ... done
    Creating example_bloonix_search ...
    Creating example_bloonix_server ...

With the command `docker ps` you can check, if all containers are started. It may take some time for Bloonix server to become `healthy`.

    CONTAINER ID        IMAGE                                                      COMMAND                  CREATED              STATUS                        PORTS                                          NAMES
    3ac691cca6a7        registry.gitlab.com/epicdocker/bloonix_server:latest       "/etc/epicsoft/entry…"   About a minute ago   Up About a minute (healthy)   0.0.0.0:443->443/tcp, 0.0.0.0:5460->5460/tcp   example_bloonix_server
    41356a4d7e71        docker.elastic.co/elasticsearch/elasticsearch:5.6.8        "/bin/bash bin/es-do…"   About a minute ago   Up About a minute             9200/tcp, 9300/tcp                             example_bloonix_search
    f5760ea3cff5        registry.gitlab.com/epicdocker/bloonix_postgresql:latest   "docker-entrypoint.s…"   About a minute ago   Up About a minute             5432/tcp                                       example_bloonix_database


###  2.4. <a name='WebUILogin'></a>WebUI Login 

Now you can reach the WebUI via the URL `https://localhost/`, the certificate is marked as unsafe because it is self-signed.

As username (Email) and password you have to enter `admin`, afterwards you will be asked to change the password.

Username (Email): `admin` / Password: `admin`


###  2.5. <a name='CreatehostviaWebUI'></a>Create host via WebUI

Click on the buttons

`Monitoring` > `Hosts` > `Plus (Create a new host)`

![add new host](images/add_new_host.png)

- In the mask for creating a host, enter the `Hostname` and `IP-Address`.  In this example, I use `MyTestHost` and` 127.0.0.1`.
- The field `Password` is already filled in with a value, this should be copied out now _(copy !!! do not remove !!!)_, because this password is needed for the agent.
- The host must be assigned to a group, select a group from the `Add the host to a user group` field. 
- Scroll down and create the host with the `submit` button.

After successfully creating the host, it will be displayed in the WebUI. Now the HostID is assigned and we can copy it, it is needed for the agent. See screentshot.

![host created](images/host_created.png)

At this time, you should have the `Host-ID` and `Host-Password`.


###  2.6. <a name='UpdateBloonixagentconfiguration'></a>Update Bloonix agent configuration 

Use an editor to open the file `simple_example_agent.yml` and insert `ID` and `password`. Here's an excerpt of what it should look like:

    [...]
      image: registry.gitlab.com/epicdocker/bloonix_agent:latest
      environment:
        - BLOONIX_AGENT_SERVER_HOST=example_bloonix_server
        - BLOONIX_AGENT_SERVER_SSL_VERIFY_MODE=none
        - BLOONIX_AGENT_HOST1_ID=1
        - BLOONIX_AGENT_HOST1_PASSWORD=t8OsNnNcl9aH6Stph5T9ZOmuyBsJGa
      networks:
        - bloonix_net
    [...]

Update the environments `BLOONIX_AGENT_HOST1_ID` and `BLOONIX_AGENT_HOST1_PASSWORD` with your values.


###  2.7. <a name='StartBloonixagent'></a>Start Bloonix agent

Now we can start the Bloonix agent. The command is similar to starting the server.

    docker-compose -f simple_example_agent.yml up -d

You have to wait again until the container becomes `healthy`. To do this use the command `docker ps`.

You can see in the logs from the agent with the following command _(this also works for the server)_.

    docker logs -f example_bloonix_agent

Now the agent should be connected to the Bloonix server.


###  2.8. <a name='Createservicestocheck_optional_'></a>Create services to check _(optional)_

To test whether the agent does his work and checks, we create a few services. To do this, you have to select the host in the WebUI and click on `Plus (Create a new service)`. This can be seen in the previous screenshot.

Here is a wide range of possible testing available. Please note that not all checks in the Docker image of the Bloonix agent works, this would require a native installtion from the agent.

![service plugins list](images/services_plugins_list.png)

For testing, I select the `Ping.Check` from the category `System, Network` and enter the IP address `127.0.0.1` in the field `Hostname or IP address`. This will ping the agent against localhost and that should always work.

As a second test I choose the `HTTP.Check` from the category `Network, Webserver, HTTP`. Here I enter in the field `URL` the value `http://example.com/`. This checks whether the page is reachable and meets certain criteria that can be fine-grained, should this be desired.

After a short moment the agent is instrumented and carries out the checks. If everything went to plan, the two checks should turn green.

![service checks green](images/example_service_checks_green.png)


###  2.9. <a name='Cleanup'></a>Cleanup

Remove the container, volumes and network.
    
    docker ps -a -q -f name=example_bloonix_ | xargs -n 1 -P 8 -I {} docker stop {}
    docker ps -a -q -f name=example_bloonix_ | xargs -n 1 -P 8 -I {} docker rm {}
    docker volume ls -q -f name=example_bloonix_ | xargs -n 1 -P 8 -I {} docker volume rm {}
    docker network rm example_bloonix_net
