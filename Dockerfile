FROM registry.gitlab.com/epicdocker/bloonix_base:1.1.4
LABEL image.name="epicsoft_bloonix_agent" \
      image.description="Docker image for Bloonix agent to work with Bloonix server" \
      maintainer="epicsoft.de" \
      maintainer.name="Alexander Schwarz <schwarz@epicsoft.de>" \
      maintainer.copyright="Copyright 2018 epicsoft.de / Alexander Schwarz" \
      license="MIT"

ENV BLOONIX_AGENT_VERSION="0.80-1" \
    BLOONIX_PLUGINS_BASIC_VERSION="0.67-1" \
    BLOONIX_PLUGINS_LINUX_VERSION="0.68-1" \
    BLOONIX_PLUGINS_WTRM_VERSION="0.22-1" \
    BLOONIX_AGENT_CONFIGURATION_SKIP=false \
    EPICSOFT_LOG_LEVEL_DEBUG=false

#### https://download.bloonix.de/repos/debian/dists/stretch/main/binary-amd64/
RUN apt-get -y update \
 && apt-get -y install procps \
                       bloonix-agent=${BLOONIX_AGENT_VERSION} \
                       bloonix-plugins-basic=${BLOONIX_PLUGINS_BASIC_VERSION} \
                       bloonix-plugins-linux=${BLOONIX_PLUGINS_LINUX_VERSION} \
                       bloonix-plugins-wtrm=${BLOONIX_PLUGINS_WTRM_VERSION} \
 && apt-get -y autoclean \
 && apt-get -y autoremove \
 && rm -rf /var/lib/apt/lists/* \
 && mv /etc/bloonix/agent/main.conf /etc/bloonix/agent/main.conf.default

COPY [ "./etc", "/etc" ]

RUN chmod +x /etc/epicsoft/*.sh \
 && chmod -R 777 /var/lib/bloonix

ENTRYPOINT [ "/etc/epicsoft/entrypoint.sh" ]

HEALTHCHECK CMD [ "/etc/epicsoft/healtcheck.sh" ]

ENV BLOONIX_AGENT_AGENTS="4" \    
    BLOONIX_AGENT_MAX_CONCURRENT_CHECKS="4" \
    BLOONIX_AGENT_MAX_CONCURRENT_HOSTS="auto" \
    BLOONIX_AGENT_POLL_INTERVAL="15" \
    BLOONIX_AGENT_USER="bloonix" \
    BLOONIX_AGENT_GROUP="bloonix" \
    BLOONIX_AGENT_SERVER_HOST="" \
    BLOONIX_AGENT_SERVER_PORT="5460" \
    BLOONIX_AGENT_SERVER_MODE="failover" \
    BLOONIX_AGENT_SERVER_USE_SSL="yes" \
    BLOONIX_AGENT_SERVER_SSL_VERIFY_MODE="peer" \
    BLOONIX_AGENT_SERVER_SSL_CA_PATH="/etc/ssl/certs" \
    BLOONIX_AGENT_USE_SUDO="unset" \
    BLOONIX_AGENT_HOST_ACTIVE_DEFAULT="yes" \
    BLOONIX_AGENT_HOST_AGENT_ID_DEFAULT="all"