#!/bin/bash

#### create configuration
if [[ $BLOONIX_AGENT_CONFIGURATION_SKIP == false ]]; then 
  echo "[INFO] $(date -Iseconds) Create Bloonix agent configuration"
  /bin/bash /etc/epicsoft/agent.conf.sh
fi

#### Bloonix agent
echo "[INFO] $(date -Iseconds) Starting Bloonix agent"
/usr/bin/bloonix-agent --pid-file /var/run/bloonix/bloonix-agent.pid --config-file /etc/bloonix/agent/main.conf

#### Bloonix logs
echo "[INFO] $(date -Iseconds) Bloonix logs ..."
while [ ! -f /var/log/bloonix/bloonix-agent.log ]; do sleep 0.1; done
tail --retry -qF /var/log/bloonix/*.log
