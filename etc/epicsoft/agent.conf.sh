#!/bin/bash

declare -g -r BLOONIX_AGENT_CONF_TPL="/etc/bloonix.template/agent.conf.tpl"
declare -g -r BLOONIX_AGENT_CONF_FILE="/etc/bloonix/agent/main.conf"

declare -g -a BLOONIX_AGENT_HOSTS=()
declare i=1

while true; do
  declare VARNAME_HOST_ID="BLOONIX_AGENT_HOST"$i"_ID"
  declare VARNAME_HOST_PASSWORD="BLOONIX_AGENT_HOST"$i"_PASSWORD"
  declare VARNAME_HOST_ACTIVE="BLOONIX_AGENT_HOST"$i"_ACTIVE"
  declare VARNAME_HOST_AGENT_ID="BLOONIX_AGENT_HOST"$i"_AGENT_ID"
  declare HOST_ID=${!VARNAME_HOST_ID}
  declare HOST_PASSWORD=${!VARNAME_HOST_PASSWORD}
  declare HOST_ACTIVE=${!VARNAME_HOST_ACTIVE}
  declare HOST_AGENT_ID=${!VARNAME_HOST_AGENT_ID}

  if [[ -n $HOST_ID && -n $HOST_PASSWORD ]]; then    
    if [[ $HOST_ACTIVE != "no" && $HOST_ACTIVE != "yes" ]]; then 
      HOST_ACTIVE=$BLOONIX_AGENT_HOST_ACTIVE_DEFAULT
    fi
    if [[ -z $HOST_AGENT_ID ]]; then 
      HOST_AGENT_ID=$BLOONIX_AGENT_HOST_AGENT_ID_DEFAULT
    fi

    if [[ $EPICSOFT_LOG_LEVEL_DEBUG == true ]]; then
      echo "[DEBUG] $(date -Iseconds) Set for host '$i' id '$HOST_ID'"
      echo "[DEBUG] $(date -Iseconds) Set for host '$i' active '$HOST_ACTIVE'"
      echo "[DEBUG] $(date -Iseconds) Set for host '$i' agent_id '$HOST_AGENT_ID'"
    fi

    read -r -d '' BLOONIX_AGENT_HOSTS[$i] << EOM
host {
    host_id $HOST_ID 
    password $HOST_PASSWORD 
    active $HOST_ACTIVE
    agent_id $HOST_AGENT_ID
}
EOM
  else 
    break
  fi

  i=$[ $i + 1 ]
done

#### load 'mo' functions
. /opt/mo/mo
#### create configuration file
cat $BLOONIX_AGENT_CONF_TPL | mo --fail-not-set > $BLOONIX_AGENT_CONF_FILE
