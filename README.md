<!-- vscode-markdown-toc -->
* 1. [Versions](#Versions)
	* 1.1. [Details](#Details)
* 2. [Requirements](#Requirements)
* 3. [Examples](#Examples)
* 4. [Environments](#Environments)
	* 4.1. [Host environments](#Hostenvironments)
		* 4.1.1. [Example of consecutive numbering](#Exampleofconsecutivenumbering)
	* 4.2. [Build environments](#Buildenvironments)
* 5. [Limitations](#Limitations)
* 6. [Configuration](#Configuration)
	* 6.1. [Custom configuration](#Customconfiguration)
	* 6.2. [Further configuration parameters](#Furtherconfigurationparameters)
* 7. [Links](#Links)
* 8. [License](#License)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->


# Bloonix agent in Docker image for Bloonix server

Docker image for Bloonix agent to work with [Bloonix server](https://bloonix-monitoring.org/).


##  1. <a name='Versions'></a>Versions

The version numbers are separate versions and have no connection with the used applications in the Docker image. In the description you can see the version numbers of the integrated applications.

`latest` [Dockerfile](https://gitlab.com/epicdocker/bloonix_agent/blob/master/Dockerfile)

`1.1.1` [Dockerfile](https://gitlab.com/epicdocker/bloonix_agent/blob/release-1.1.1/Dockerfile)

`1.1.0` [Dockerfile](https://gitlab.com/epicdocker/bloonix_agent/blob/release-1.1.0/Dockerfile)

`1.0.0` [Dockerfile](https://gitlab.com/epicdocker/bloonix_agent/blob/release-1.0.0/Dockerfile)


###  1.1. <a name='Details'></a>Details

`1.1.1` `latest`

- See version `1.1.0`
- Used the [Bloonix base image](https://gitlab.com/epicdocker/bloonix_base/tree/release-1.1.4/) in version `1.1.4` as base image.

`1.1.0` 

- Used the [Bloonix base image](https://gitlab.com/epicdocker/bloonix_base/tree/release-1.1.2/) in version `1.1.2` as base image.
- Used Bloonix agent in version `0.80-1` from [Bloonix repository](https://download.bloonix.de/repos/debian/dists/stretch/main/binary-amd64/).
- Used Bloonix plugins-basic in version `0.67-1` from [Bloonix repository](https://download.bloonix.de/repos/debian/dists/stretch/main/binary-amd64/).
- Used Bloonix plugins-linux in version `0.68-1` from [Bloonix repository](https://download.bloonix.de/repos/debian/dists/stretch/main/binary-amd64/).
- Used Bloonix plugins-wtrm in version `0.22-1` from [Bloonix repository](https://download.bloonix.de/repos/debian/dists/stretch/main/binary-amd64/).

`1.0.0`

- Used the [Bloonix base image](https://gitlab.com/epicdocker/bloonix_base/tree/release-1.1.2/) in version `1.1.2` as base image.
- Used Bloonix agent in version `0.80-1` from [Bloonix repository](https://download.bloonix.de/repos/debian/dists/stretch/main/binary-amd64/).


##  2. <a name='Requirements'></a>Requirements

- Docker installed - https://www.docker.com/get-docker
- Optional: Docker Swarm initialized - https://docs.docker.com/engine/swarm/ - Some examples use Docker Swarm, but are not required.
- Optional: Docker Compose installed (is needed for examples) - https://docs.docker.com/compose/


##  3. <a name='Examples'></a>Examples

See the subdirectory [examples](https://gitlab.com/epicdocker/bloonix_agent/tree/master/examples)

- [Simple example to add an agent](https://gitlab.com/epicdocker/bloonix_agent/tree/master/examples/simple_example_add_agent.md)


##  4. <a name='Environments'></a>Environments

`BLOONIX_AGENT_SERVER_HOST` **required**

*default:* _empty_

The host to connect to the bloonix server.

`BLOONIX_AGENT_SERVER_PORT`

*default:* 5460

The port to connect to the bloonix server.

`BLOONIX_AGENT_HOST_ACTIVE_DEFAULT`

*default:* yes

Default value for variable `BLOONIX_AGENT_HOST[X]_ACTIVE`, if not specified.

`BLOONIX_AGENT_HOST_AGENT_ID_DEFAULT`

*default:* all

Default value for variable `BLOONIX_AGENT_HOST[X]_AGENT_ID`, if not specified.


###  4.1. <a name='Hostenvironments'></a>Host environments

Multiple hosts can be specified with consecutive numbering. The numbering starts at `1` and ends as soon as a HostID can not be found under the number.

**IMPORTANT**: If a host is deleted in the consecutive numbering, so that a gap is created, the processing stops at this point. Use the `BLOONIX_AGENT_HOST[X]_ACTIVE` parameter to disable a host.

`BLOONIX_AGENT_HOST1_ID` **required**

*default:* _empty_

This is the host id for which the agent executes the checks and collects the statistic data. You can find the host id in the Bloonix WebGUI.

`BLOONIX_AGENT_HOST1_PASSWORD` **required**

*default:* _empty_

This is the password for the given host id. You can find the password in the Bloonix WebGUI. The agent must be authorized to send data for the given host id.

`BLOONIX_AGENT_HOST1_ACTIVE`

*default:* yes

Enable or disable the check of this host with "yes" or "no".

`BLOONIX_AGENT_HOST1_AGENT_ID`

*default:* all

This parameter handles for which part of a host its collect the data.


####  4.1.1. <a name='Exampleofconsecutivenumbering'></a>Example of consecutive numbering

    BLOONIX_AGENT_HOST1_ID
    BLOONIX_AGENT_HOST1_PASSWORD
    BLOONIX_AGENT_HOST1_ACTIVE
    BLOONIX_AGENT_HOST1_AGENT_ID

    BLOONIX_AGENT_HOST2_ID
    BLOONIX_AGENT_HOST2_PASSWORD
    BLOONIX_AGENT_HOST2_ACTIVE
    BLOONIX_AGENT_HOST2_AGENT_ID

    BLOONIX_AGENT_HOST[X]_ID
    BLOONIX_AGENT_HOST[X]_PASSWORD
    BLOONIX_AGENT_HOST[X]_ACTIVE
    BLOONIX_AGENT_HOST[X]_AGENT_ID


###  4.2. <a name='Buildenvironments'></a>Build environments

`BLOONIX_AGENT_VERSION`

*default:* see version details

_Used only when building Docker image_

Specifies the Bloonix agent version to install.


##  5. <a name='Limitations'></a>Limitations

- Docker container can not collect all values from the host, e.g. Memory.
- Agent ID must be set to 'all' for the checks to work


##  6. <a name='Configuration'></a>Configuration

The configuration templates are located in directory `/etc/bloonix.template` and are regenerated each time when the container is started.


###  6.1. <a name='Customconfiguration'></a>Custom configuration

To change existing configuration, you can overwrite the directory `/etc/bloonix.template` with your own templates. The current templates, see [Source code](https://gitlab.com/epicdocker/bloonix_agent/tree/master/etc/bloonix.template), can you take and change. When the container starting, configurations are created from the templates.


###  6.2. <a name='Furtherconfigurationparameters'></a>Further configuration parameters

For a description, see the template or the Bloonix configuration file.

`BLOONIX_AGENT_AGENTS`

*default:* 4

`BLOONIX_AGENT_MAX_CONCURRENT_CHECKS`

*default:* 4

`BLOONIX_AGENT_MAX_CONCURRENT_HOSTS`

*default:* auto

`BLOONIX_AGENT_POLL_INTERVAL`

*default:* 15

`BLOONIX_AGENT_USER`

*default:* bloonix

`BLOONIX_AGENT_GROUP`

*default:* bloonix

`BLOONIX_AGENT_SERVER_MODE`

*default:* failover

`BLOONIX_AGENT_SERVER_USE_SSL`

*default:* yes

`BLOONIX_AGENT_SERVER_SSL_VERIFY_MODE`

*default:* peer

`BLOONIX_AGENT_SERVER_SSL_CA_PATH`

*default:* /etc/ssl/certs

`BLOONIX_AGENT_USE_SUDO`

*default:* unset


##  7. <a name='Links'></a>Links 

- Bloonix server Docker image - https://gitlab.com/epicdocker/bloonix_server
- Bloonix server Docker registry - https://gitlab.com/epicdocker/bloonix_server/container_registry
- Bloonix PostgreSQL Docker image _(preinitialised)_ - https://gitlab.com/epicdocker/bloonix_postgresql
- Bloonix agent Docker image - https://gitlab.com/epicdocker/bloonix_agent
- Bloonix satellite Docker image - https://gitlab.com/epicdocker/bloonix_satellite
- Bloonix base Docker image - https://gitlab.com/epicdocker/bloonix_base
- Bloonix Repository - https://download.bloonix.de/repos/debian/dists/stretch/main/binary-amd64/
- Bloonix OpenSource Monitoring Software - https://bloonix-monitoring.org/


##  8. <a name='License'></a>License

MIT License see [LICENSE](https://gitlab.com/epicdocker/bloonix_agent/blob/master/LICENSE)